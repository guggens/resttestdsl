package com.guggens.resttestdsl

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.junit4.SpringRunner
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.ComparisonFailure
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ResttestdslApplicationTests {

	@Autowired
	lateinit var context: ApplicationContext

	@Test
	fun callGet() {
		get<Example>("/example") {
			status(200)
			body {
				assertThat(count).isEqualTo(3)
			}
		}
	}

	@Test
	fun callGetList() {
		get<List<Example>>("/examples") {
			status(200)
			body {
				assertThat(size).isEqualTo(1)
			}
		}
	}

	@Test(expected = ComparisonFailure::class)
	fun callGetStatusIsVerified() {
		get<Example>("/example") {
			status(203)
			fail("exception expected")
		}
	}

	@Test(expected = ComparisonFailure::class)
	fun callGetBodyIsVerified() {
		post<Example>("/example", Input(127)) {
			body {
				assertThat(count).isEqualTo(1)
				fail("exception expected")
			}
		}
	}

	@Test
	fun callPost() {
		post<Example>("/example", Input(127)) {
			status(202)
			body {
				assertThat(count).isEqualTo(3)
			}
		}
	}

}
