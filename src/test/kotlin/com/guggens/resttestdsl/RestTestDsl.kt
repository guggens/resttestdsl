package com.guggens.resttestdsl


import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*



fun <T: Any> ResponseEntity<T>.status(status: HttpStatus) {
    assertThat(statusCode).isEqualTo(status)
}

fun <T: Any> ResponseEntity<T>.status(status: Int) {
    assertThat(statusCodeValue).isEqualTo(status)
}


fun <T : Any> ResponseEntity<T>.body(block: T.() -> Unit) {
    if (body != null) {
        body!!.block()
    } else {
        fail("body is null!")
    }
}

fun <T : Any> ResponseEntity<T>.headers(block: HttpHeaders.() -> Unit) {
    headers.block()
}

inline fun <reified T : Any> ResttestdslApplicationTests.post(
        url: String,
        body: Any? = null,
        headers: HttpHeaders = HttpHeaders.EMPTY,
        block: ResponseEntity<T>.() -> Unit = {}): ResponseEntity<T> {

    if (body is HttpHeaders) {
        throw IllegalArgumentException("HttpHeaders passed as body!")
    }

    val http = context.getBean(TestRestTemplate::class.java)

    val responseEntity = http.exchange(url, HttpMethod.POST, HttpEntity(body, headers), T::class.java)
    responseEntity.block()
    return responseEntity
}

inline fun <reified T : Any> ResttestdslApplicationTests.put(
        url: String,
        body: Any? = null,
        headers: HttpHeaders = HttpHeaders.EMPTY,
        block: ResponseEntity<T>.() -> Unit = {}): ResponseEntity<T> {

    if (body is HttpHeaders) {
        throw IllegalArgumentException("HttpHeaders passed as body!")
    }

    val http = context.getBean(TestRestTemplate::class.java)

    val responseEntity = http.exchange(url, HttpMethod.PUT, HttpEntity(body, headers), T::class.java)
    responseEntity.block()
    return responseEntity
}

inline fun <reified T: Any> ResttestdslApplicationTests.get(
        url: String,
        headers: HttpHeaders = HttpHeaders.EMPTY,
        block: ResponseEntity<T>.()->Unit = {}) : ResponseEntity<T> {

    val http = context.getBean(TestRestTemplate::class.java)

    val responseEntity = http.exchange(url, HttpMethod.GET, HttpEntity<Any>(headers), object : ParameterizedTypeReference<T>() {})
    responseEntity.block()
    return responseEntity
}

inline fun <reified T : Any> ResttestdslApplicationTests.delete(
        url: String,
        body: Any? = null,
        headers: HttpHeaders = HttpHeaders.EMPTY,
        block: ResponseEntity<T>.() -> Unit = {}): ResponseEntity<T> {

    if (body is HttpHeaders) {
        throw IllegalArgumentException("HttpHeaders passed as body!")
    }

    val http = context.getBean(TestRestTemplate::class.java)

    val responseEntity = http.exchange(url, HttpMethod.DELETE, HttpEntity(body, headers), T::class.java)
    responseEntity.block()
    return responseEntity
}