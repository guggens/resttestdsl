package com.guggens.resttestdsl

import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController


@RestController
class ExampleController {


    @RequestMapping(
            path = ["/example"],
            method = [RequestMethod.GET],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE]
    )
    fun getExample(): Example {
        return Example()
    }

    @RequestMapping(
            path = ["/examples"],
            method = [RequestMethod.GET],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE]
    )
    fun getExamples(): List<Example> {
        return listOf(Example())
    }

    @RequestMapping(
            path = ["/example"],
            method = [RequestMethod.POST],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE],
            consumes = [org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE]
    )
    fun postExample(@RequestBody input: Input): ResponseEntity<Example> {
        return ResponseEntity.accepted().body(Example(input.toString()))
    }

    @RequestMapping(
            path = ["/example"],
            method = [RequestMethod.PUT],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE],
            consumes = [org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE]
    )
    fun putExample(@RequestBody input: Input): ResponseEntity<Example> {
        return ResponseEntity.accepted().body(Example(input.toString()))
    }

    @RequestMapping(
            path = ["/example"],
            method = [RequestMethod.DELETE],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE],
            consumes = [org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE]
    )
    fun deleteExample(): ResponseEntity<Example> {
        return ResponseEntity.noContent().build()
    }
}