package com.guggens.resttestdsl

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ResttestdslApplication

fun main() {
    runApplication<ResttestdslApplication>()
}
