package com.guggens.resttestdsl

data class Example (
        val name: String ="example",
        val count: Int = 3
)